package task04_1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {

    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        StringContainer<String> stringContainer = new StringContainer<>(1);
        stringContainer.add("i");
        stringContainer.add("am");
        stringContainer.add("Illia");
        logger.info(stringContainer);
    }
}
