package task04_1;

public class StringContainer<T extends String> {
    private int size;

    private Object[] data;

    public StringContainer(int size) {
        data = new Object[size];
    }

    public StringContainer() {
        size = 0;
        data = new Object[size];
    }

    private void expand() {
        Object[] tempArray = new Object[data.length];
        for (int i = 0; i < data.length; i++) {
            tempArray[i] = data[i];
        }
        int newSize = (int) ((size + 1) * 1.6);
        data = new Object[newSize];
        for (int i = 0; i < tempArray.length; i++) {
            data[i] = tempArray[i];
        }
    }

    public void add(T element) {
        if (size == data.length) {
            expand();
        }
        data[size] = element;
        size++;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < size; i++) {
            stringBuilder.append(data[i].toString()).append(" ");
        }
        return stringBuilder.toString();
    }
}
