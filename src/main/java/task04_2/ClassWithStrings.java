package task04_2;

public class ClassWithStrings implements Comparable {
    private String firstString;
    private String secondString;

    public ClassWithStrings() {
        firstString = "first";
        secondString = "second";
    }

    public ClassWithStrings(String first, String second) {
        firstString = first;
        secondString = second;
    }

    @Override
    public int compareTo(Object o) {
        return firstString.length() > ((ClassWithStrings) o).getFirstString().length() ? -1 : 0;
    }

    public String getFirstString() {
        return firstString;
    }

    public void setFirstString(String firstString) {
        this.firstString = firstString;
    }

    public String getSecondString() {
        return secondString;
    }

    public void setSecondString(String secondString) {
        this.secondString = secondString;
    }

    @Override
    public String toString() {
        return firstString + " " + secondString;
    }
}
