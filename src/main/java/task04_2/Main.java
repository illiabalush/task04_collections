package task04_2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        List<ClassWithStrings> list = new ArrayList<>();
        list.add(new ClassWithStrings("Ukraine", "Kiev"));
        list.add(new ClassWithStrings("USA", "Washington D.C."));
        list.add(new ClassWithStrings("Poland", "Warsaw"));
        Object[] classWithStrings = list.toArray();
        Arrays.sort(classWithStrings);
        logger.info(Arrays.toString(classWithStrings));
        list.sort(((o1, o2) -> o1.getSecondString().length() > o2.getSecondString().length() ? -1 : 0));
        logger.info(list);
    }
}
