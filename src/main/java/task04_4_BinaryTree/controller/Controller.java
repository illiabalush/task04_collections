package task04_4_BinaryTree.controller;

import task04_4_BinaryTree.model.BinaryTreeMap;

public abstract class Controller {
    protected BinaryTreeMap<Integer, String> binaryTreeMap;

    public BinaryTreeMap<Integer, String> getBinaryTreeMap() {
        return binaryTreeMap;
    }
}
