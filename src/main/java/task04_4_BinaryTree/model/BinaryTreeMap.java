package task04_4_BinaryTree.model;

import java.util.*;

public class BinaryTreeMap<K extends Comparable<K>, V> implements Map<K, V> {

    private Node<K, V> root;
    private int size = 0;

    private Node<K, V> addToTree(Node<K, V> current, Node<K, V> parent, K key, V value) {
        if (current == null) {
            return new Node<>(parent, key, value);
        }
        if (key.compareTo(current.key) < 0) {
            current.left = addToTree(current.left, current, key, value);
        } else if (key.compareTo(current.key) > 0) {
            current.right = addToTree(current.right, current, key, value);
        } else if (key.compareTo(current.key) == 0) {
            current.value = value;
        }
        return current;
    }

    private void add(K key, V value) {
        root = addToTree(root, null, key, value);
        size++;
    }

    public final class Node<K extends Comparable<K>, V> implements Comparable<K> {
        K key;
        V value;
        Node<K, V> left;
        Node<K, V> right;
        Node<K, V> parent;

        Node(Node<K, V> parent, K key, V value) {
            this.key = key;
            this.value = value;
            this.parent = parent;
        }

        @Override
        public int compareTo(K o) {
            return key.compareTo(o);
        }

        @Override
        public String toString() {
            return key + " | " + value;
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        return searchKey(root, (K) key) != null;
    }

    private Node<K, V> searchKey(Node<K, V> node, K key) {
        if (node == null) {
            return null;
        }
        if (node.key.equals(key)) {
            return node;
        }
        return (node.key.compareTo(key) > 0)
                ? searchKey(node.left, key)
                : searchKey(node.right, key);
    }

    @Override
    public boolean containsValue(Object value) {
        Node<K, V> findNode = new Node<>(null, null, null);
        searchValue(root, value, findNode);
        return findNode.value.equals(value);
    }

    private void searchValue(Node<K, V> node, Object value, Node<K, V> findNode) {
        if (node != null) {
            if (node.value == value) {
                findNode.parent = node.parent;
                findNode.key = node.key;
                findNode.value = node.value;
                return;
            }
            searchValue(node.left, value, findNode);
            searchValue(node.right, value, findNode);
        }
    }

    @Override
    public V get(Object key) {
        return searchKey(root, (K) key).value;
    }

    @Override
    public V put(K key, V value) {
        add(key, value);
        return value;
    }

    @Override
    public V remove(Object key) {
        return deleteNode(searchKey(root, (K) key)).value;
    }

    private Node<K, V> deleteNode(Node<K, V> node) {
        if (node.left == null && node.right == null) {
            deleteWithoutBranches(node);
        } else if (node.left == null || node.right == null) {
            deleteWithOneBranches(node);
        } else {
            deleteWithTwoBranches(node);
        }
        size--;
        return (!isRoot(node)) ? node.parent : node;
    }

    private void deleteWithoutBranches(Node<K, V> node) {
        if (isRoot(node)) {
            root = null;
        } else if (node.parent.key.compareTo(node.key) > 0) {
            node.parent.left = null;
        } else {
            node.parent.right = null;
        }
    }

    private boolean isRoot(Node<K, V> node) {
        return node.parent == null;
    }

    private void deleteWithOneBranches(Node<K, V> node) {
        if (isRoot(node)) {
            if (node.left != null) {
                root = node.left;
            } else {
                root = node.right;
            }
            root.parent = null;
        } else if (node.left != null) {
            if (node.parent.key.compareTo(node.key) > 0) {
                node.left.parent = node.parent;
                node.parent.left = node.left;
            } else {
                node.left.parent = node.parent;
                node.parent.right = node.left;
            }
        } else {
            if (node.parent.key.compareTo(node.key) > 0) {
                node.right.parent = node.parent;
                node.parent.left = node.right;
            } else {
                node.right.parent = node.parent;
                node.parent.right = node.right;
            }
        }
    }

    private void deleteWithTwoBranches(Node<K, V> node) {
        Node<K, V> smallestNode = getSmallestNode(node.right);
        if (smallestNode.right != null && smallestNode.parent != node) {
            smallestNode.right.parent = smallestNode.parent;
            smallestNode.parent.left = smallestNode.right;
            transferNodeValues(smallestNode, node);
        } else if (smallestNode.right == null && smallestNode.parent == node) {
            node.left.parent = smallestNode;
            smallestNode.left = smallestNode.parent.left;
            node.right = null;
            transferNodeValues(smallestNode, node);
        } else if (smallestNode.right == null) {
            smallestNode.parent.left = null;
            transferNodeValues(smallestNode, node);
        } else {
            smallestNode.right.parent = smallestNode.parent;
            smallestNode.parent.right = smallestNode.right;
            transferNodeValues(smallestNode, node);
        }
    }

    private Node<K, V> getSmallestNode(Node<K, V> node) {
        return node.left == null ? node : getSmallestNode(node.left);
    }

    private void transferNodeValues(Node<K, V> from, Node<K, V> to) {
        to.key = from.key;
        to.value = from.value;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
    }

    @Override
    public void clear() {
        size = 0;
        root = null;
    }

    @Override
    public Set<K> keySet() {
        Set<K> keySet = new LinkedHashSet<>();
        fillKeySet(root, keySet);
        return keySet;
    }

    private void fillKeySet(Node<K, V> node, Set<K> keySet) {
        if (node != null) {
            fillKeySet(node.left, keySet);
            keySet.add(node.key);
            fillKeySet(node.right, keySet);
        }
    }

    @Override
    public Collection<V> values() {
        List<V> listValues = new ArrayList<>();
        fillValuesList(root, listValues);
        return listValues;
    }

    private void fillValuesList(Node<K, V> node, List<V> listValues) {
        if (node != null) {
            fillValuesList(node.left, listValues);
            listValues.add(node.value);
            fillValuesList(node.right, listValues);
        }
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return null;
    }

    private void buildTreeString(Node<K, V> node, StringBuilder treeBuilder) {
        if (node != null) {
            buildTreeString(node.left, treeBuilder);
            treeBuilder.append(" [").append(node).append("]");
            buildTreeString(node.right, treeBuilder);
        }
    }

    @Override
    public String toString() {
        StringBuilder treeBuilder = new StringBuilder();
        buildTreeString(root, treeBuilder);
        return treeBuilder.toString();
    }
}
