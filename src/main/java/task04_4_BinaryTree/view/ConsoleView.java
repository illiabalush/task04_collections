package task04_4_BinaryTree.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task04_4_BinaryTree.controller.Controller;
import task04_4_BinaryTree.controller.ControllerImpl;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ConsoleView {
    private Map<String, String> menu;
    private Map<String, Printable> methodsOnMenu;
    private Scanner input;
    private Controller controller;
    private static Logger logger = LogManager.getLogger(ConsoleView.class);

    public ConsoleView() {
        controller = new ControllerImpl();
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - show tree map");
        menu.put("2", "  2 - add element");
        menu.put("3", "  3 - remove element");
        menu.put("4", "  4 - contains element");
        menu.put("5", "  5 - show size");
        menu.put("Q", "  Q - quit");
        methodsOnMenu = new LinkedHashMap<>();
        methodsOnMenu.put("1", this::showTree);
        methodsOnMenu.put("2", this::addElement);
        methodsOnMenu.put("3", this::removeElement);
        methodsOnMenu.put("4", this::containsElement);
        methodsOnMenu.put("5", this::getSize);
    }

    private void showMenu() {
        logger.info("MENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void start() {
        String keyMenu;
        do {
            showMenu();
            input = new Scanner(System.in);                 //?????
            logger.info("Please, select menu point:");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsOnMenu.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void showTree() {
        logger.info(controller.getBinaryTreeMap());
    }

    private void addElement() {
        logger.info("Enter key: ");
        Integer key = input.nextInt();
        logger.info("Enter value: ");
        input = new Scanner(System.in);                 //?????
        String value = input.nextLine();
        controller.getBinaryTreeMap().put(key, value);
    }

    private void removeElement() {
        logger.info("Enter key: ");
        Integer key = input.nextInt();
        input = new Scanner(System.in);                 //?????
        logger.info("Enter value: ");
        String value = input.nextLine();
        controller.getBinaryTreeMap().remove(key, value);
    }

    private void containsElement() {
        logger.info("Enter key: ");
        input = new Scanner(System.in);                 //?????
        Integer key = input.nextInt();
        logger.info(controller.getBinaryTreeMap().containsKey(key));
    }

    private void getSize() {
        logger.info(controller.getBinaryTreeMap().size());
    }
}
